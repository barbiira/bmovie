import { Movie } from "../models/movie";

export function getMovies(movie, cb) {
  var xhr = new XMLHttpRequest();
  xhr.open(
    "GET",
    //?s=.... || &=P...&n=1 <-- Query param
    "https://fake-movie-database-api.herokuapp.com/api?s=".concat(movie)
  );

  xhr.addEventListener("loadend", function () {
    //Con parse trasformo quella stringa (http) in una sequenza dati reale per JS, in questo caso
    //ad esempio abbiamo trasformato l'http in un array.
    var data = JSON.parse(xhr.response);
    var data = JSON.parse(xhr.response);

    var movies = data.Search.map(function (m) {
      return new Movie(m.imdbID, m.Title, m.Poster);
    });

    cb(movies);
  });

  xhr.send();
}

export function firstB(isFirst) {
  if (isFirst) {
    film === "0" ? "batman" : film;
  }
}

export function sliceMovies(movies, from, to) {
  var idx = from;
  var goOn = true;
  var ret = [];

  do {
    ret.push(movies[idx]);

    if (idx === to) {
      goOn = false;
    } else if (idx < to || (idx > to && idx < movies.length - 1)) {
      idx++;
    } else {
      idx = 0;
    }
  } while (goOn);

  return ret;
}
