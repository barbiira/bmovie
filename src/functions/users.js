import { User } from "../models/user";

export function getUsers(cb) {
  var xhr = new XMLHttpRequest();

  xhr.open("GET", "https://jsonplaceholder.typicode.com/users");

  xhr.addEventListener("loadend", function () {
    var data = JSON.parse(xhr.response);

    // var users = data.map(function (u) {
    //   return new User(u.id, u.email, u.username);

    //Per ogni elemento dell'array applica questa funzione
    //La u (prettier) l'ha messa fra parentesi perchè così non è ambiguo il codice
    //prendo u (parametro) che è l'oggetto su cui itero , mi crea un nuovo User
    //non metto return perchè le graffe non ci sono
    const users = data.map((u) => new User(u.id, u.email, u.username));

    cb(users);
  });

  xhr.send();
}
